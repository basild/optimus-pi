from unittest import TestCase
from unittest.mock import MagicMock

from optimuspi.control.keyboard_control import KeyboardControl


class TestKeyboardControl(TestCase):
    LEFT = "L"
    RIGHT = "R"
    FORWARD = "F"
    BACKWARD = "B"
    SPEED_INC = "SI"
    SPEED_DEC = "SD"

    def setUp(self):
        self.mock_robot = MagicMock()
        self.system_under_test = KeyboardControl(robot=self.mock_robot, left=TestKeyboardControl.LEFT,
                                                 right=TestKeyboardControl.RIGHT, forward=TestKeyboardControl.FORWARD,
                                                 backward=TestKeyboardControl.BACKWARD,
                                                 speed_inc=TestKeyboardControl.SPEED_INC,
                                                 speed_dec=TestKeyboardControl.SPEED_DEC)

    def test_forward(self):
        self.system_under_test.on_key_down(TestKeyboardControl.FORWARD)
        self.mock_robot.forward.assert_called()

    def test_backward(self):
        self.system_under_test.on_key_down(TestKeyboardControl.BACKWARD)
        self.mock_robot.backward.assert_called()

    def test_spin_clockwise(self):
        self.system_under_test.on_key_down(TestKeyboardControl.RIGHT)
        self.mock_robot.spin_clockwise.assert_called()

    def test_spin_counterclockwise(self):
        self.system_under_test.on_key_down(TestKeyboardControl.LEFT)
        self.mock_robot.spin_counterclockwise.assert_called()

    def test_forward_left(self):
        self.system_under_test.on_key_down(TestKeyboardControl.FORWARD)
        self.system_under_test.on_key_down(TestKeyboardControl.LEFT)
        self.mock_robot.forward_left.assert_called()

    def test_forward_right(self):
        self.system_under_test.on_key_down(TestKeyboardControl.FORWARD)
        self.system_under_test.on_key_down(TestKeyboardControl.RIGHT)
        self.mock_robot.forward_right.assert_called()

    def test_backward_left(self):
        self.system_under_test.on_key_down(TestKeyboardControl.BACKWARD)
        self.system_under_test.on_key_down(TestKeyboardControl.LEFT)
        self.mock_robot.backward_left.assert_called()

    def test_backward_right(self):
        self.system_under_test.on_key_down(TestKeyboardControl.BACKWARD)
        self.system_under_test.on_key_down(TestKeyboardControl.RIGHT)
        self.mock_robot.backward_right.assert_called()

    def test_speed(self):
        self.system_under_test.on_key_up(TestKeyboardControl.SPEED_DEC)
        self.system_under_test.on_key_down(TestKeyboardControl.FORWARD)
        self.system_under_test.on_key_up(TestKeyboardControl.FORWARD)
        self.mock_robot.decrease_speed.assert_called()

        self.system_under_test.on_key_up(TestKeyboardControl.SPEED_INC)
        self.system_under_test.on_key_down(TestKeyboardControl.FORWARD)
        self.mock_robot.increase_speed.assert_called()
