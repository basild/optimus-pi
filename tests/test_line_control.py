import logging
from typing import Type
from unittest import TestCase
from unittest.mock import MagicMock

from gpiozero import Device
from gpiozero.pins.mock import MockFactory, MockPWMPin

from optimuspi.control.line_control import OnlyStraddleSensorStrategy, StraddleAndGuideSensorStrategy
from optimuspi.robot.optimus_pi import LineSensorGroup

FORMAT = '%(asctime)-15s %(module)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)


class SensorTestHelper:
    def __init__(self, strategy: Type = None, positions: list = None):
        Device.pin_factory = MockFactory(pin_class=MockPWMPin)
        self.mock_pin_factory = Device.pin_factory
        self.mock_robot = MagicMock(name="mock.robot")

        self.mock_left = MagicMock(name="mock.left")
        self.mock_right = MagicMock(name="mock.right")
        self.mock_centre_front = MagicMock(name="mock.centre_front")

        self.all_sensors = {"left": self.mock_left, "right": self.mock_right, "centre-front": self.mock_centre_front}

        self.mock_line_sensors = LineSensorGroup(self.mock_robot)
        self.mock_line_sensors.sensors = {}
        for position in positions:
            self.mock_line_sensors.sensors[position] = self.all_sensors[position]

        self.mock_robot.line_sensors = self.mock_line_sensors
        self.mock_distance_sensor = MagicMock()
        self.mock_robot.distance_sensor = self.mock_distance_sensor
        self.mock_robot.distance_sensor.is_too_close.return_value = False

        self.strategy = strategy(self.mock_robot)

    def trigger_sensor_state(self, sensor_states: dict = None):

        for position, state in sensor_states.items():
            self.mock_line_sensors.sensors[position].is_active = state

        self.strategy.sensor_status_changed(self.mock_robot, self.mock_left)

    def verify_sensor_action(self, sensor_states: dict, expected_method: str):
        self.trigger_sensor_state(sensor_states=sensor_states)
        getattr(self.mock_robot, expected_method).assert_called()


class TestOnlyStraddleSensorStrategy(TestCase):
    def setUp(self):
        self.helper = SensorTestHelper(OnlyStraddleSensorStrategy, ["left", "right"])

    def test_all_off_straight_ahead(self):
        self.helper.verify_sensor_action({"left": False, "right": False}, "forward")

    def test_left_on_turn_left(self):
        self.helper.verify_sensor_action({"left": True, "right": False}, "forward_left")

    def test_right_on_turn_right(self):
        self.helper.verify_sensor_action({"left": False, "right": True}, "forward_right")

    def test_invalid_state_stop(self):
        self.helper.verify_sensor_action({"left": True, "right": True}, "stop")


class TestStraddleAndGuideSensorStrategy(TestCase):
    def setUp(self):
        self.helper = SensorTestHelper(StraddleAndGuideSensorStrategy, ["left", "right", "centre-front"])

    def test_guide_on_straight_ahead(self):
        self.helper.verify_sensor_action({"left": False, "right": False, "centre-front": True}, "forward")

    def test_left_centre_on_turn_left(self):
        self.helper.verify_sensor_action({"left": True, "right": False, "centre-front": True}, "forward_left")

    def test_only_left_on_spin_left(self):
        self.helper.verify_sensor_action({"left": True, "right": False, "centre-front": False}, "spin_counterclockwise")

    def test_right_centre_on_turn_right(self):
        self.helper.verify_sensor_action({"left": False, "right": True, "centre-front": True}, "forward_right")

    def test_only_right_on_spin_right(self):
        self.helper.verify_sensor_action({"left": False, "right": True, "centre-front": False}, "spin_clockwise")

    def test_invalid_state_stop(self):
        self.helper.verify_sensor_action({"left": True, "right": True}, "stop")
