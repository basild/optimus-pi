from unittest import TestCase

from gpiozero import Device
from gpiozero.pins.mock import MockFactory, MockPWMPin
from optimuspi.robot.optimus_pi import OptimusPi


class TestPropulsion(TestCase):
    LEFT_MOTOR_A = 1
    LEFT_MOTOR_B = 2
    RIGHT_MOTOR_A = 3
    RIGHT_MOTOR_B = 4

    def setUp(self):
        Device.pin_factory = MockFactory(pin_class=MockPWMPin)
        self.mock_pin_factory = Device.pin_factory
        self.system_under_test = OptimusPi((TestPropulsion.LEFT_MOTOR_A, TestPropulsion.LEFT_MOTOR_B),
                                           (TestPropulsion.RIGHT_MOTOR_A, TestPropulsion.RIGHT_MOTOR_B),
                                           motor_pwm=True)
        self.system_under_test.set_speed(0.75)

    def tearDown(self):
        self.mock_pin_factory.reset()

    def test_set_speed_within_range(self):
        self.system_under_test.set_speed(0.5)
        assert self.system_under_test.speed == 0.5

    def test_set_speed_under_range(self):
        self.system_under_test.set_speed(-1)
        assert self.system_under_test.speed == 0

    def test_set_speed_over_range(self):
        self.system_under_test.set_speed(2)
        assert self.system_under_test.speed == 1

    def test_increase_speed_within_range(self):
        self.system_under_test.speed = 0.5
        self.system_under_test.increase_speed()
        assert self.system_under_test.speed == 0.5 + OptimusPi.SPEED_RES

    def test_increase_speed_over_range(self):
        self.system_under_test.speed = 1
        self.system_under_test.increase_speed()
        self.assertEqual(self.system_under_test.speed,  1)

    def test_decrease_speed_under_range(self):
        self.system_under_test.speed = 0
        self.system_under_test.decrease_speed()
        self.assertEqual(self.system_under_test.speed,  0)

    def test_decrease_speed_within_range(self):
        self.system_under_test.speed = 0.5
        self.system_under_test.decrease_speed()
        self.assertEqual(self.system_under_test.speed,  0.5 - OptimusPi.SPEED_RES)

    def _verify_pins(self, left_a=None, left_b=None, right_a=None, right_b=None):
        self._verify_pin(TestPropulsion.LEFT_MOTOR_A, left_a)
        self._verify_pin(TestPropulsion.LEFT_MOTOR_B, left_b)
        self._verify_pin(TestPropulsion.RIGHT_MOTOR_A, right_a)
        self._verify_pin(TestPropulsion.RIGHT_MOTOR_B, right_b)

    def _verify_pin(self, pin=None, values=None):
        self.assertEqual(self.mock_pin_factory.pin(pin)._get_state(), values)

    def _verify_moving(self, is_moving: bool):
        self.assertEqual(is_moving, self.system_under_test.is_moving())

    def test_stop(self):
        self.system_under_test.stop()

        self._verify_pins(0, 0, 0, 0)
        self._verify_moving(False)

    def test_move_forward_full_speed(self):
        self.system_under_test.forward()

        self._verify_pins(self.system_under_test.speed, 0, self.system_under_test.speed, 0)
        self._verify_moving(True)

    def test_move_backward_full_speed(self):
        self.system_under_test.backward()

        self._verify_pins(0, self.system_under_test.speed, 0, self.system_under_test.speed)
        self._verify_moving(True)

    def test_spin_clockwise_full_speed(self):
        self.system_under_test.spin_clockwise()

        self._verify_pins(self.system_under_test.speed, 0, 0, self.system_under_test.speed)
        self._verify_moving(True)

    def test_spin_counterclockwise(self):
        self.system_under_test.spin_counterclockwise()

        self._verify_pins(0, self.system_under_test.speed, self.system_under_test.speed, 0)
        self._verify_moving(True)

    def test_forward_left(self):
        self.system_under_test.forward_left()

        self._verify_pins(0, 0, self.system_under_test.speed, 0)
        self._verify_moving(True)

    def test_forward_right(self):
        self.system_under_test.forward_right()

        self._verify_pins(self.system_under_test.speed, 0, 0, 0)
        self._verify_moving(True)

    def test_backward_left(self):
        self.system_under_test.backward_left()

        self._verify_pins(0, 0, 0, self.system_under_test.speed)
        self._verify_moving(True)

    def test_backward_right(self):
        self.system_under_test.backward_right()

        self._verify_pins(0, self.system_under_test.speed, 0, 0)
        self._verify_moving(True)
