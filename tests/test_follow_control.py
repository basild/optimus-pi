import logging
from time import sleep, time
from unittest import TestCase
from unittest.mock import MagicMock, Mock

from optimuspi.control.follow_control import FollowControl
from optimuspi.robot.optimus_pi import RobotDistanceSensor

FORMAT = '%(asctime)-15s %(module)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)


class TestFollowControl(TestCase):
    SCAN_SLEEP = 0.2

    def setUp(self):
        self.mock_distance_sensor = Mock()
        self.mock_robot = MagicMock()

        self.mock_robot.distance_sensor = self.mock_distance_sensor
        self.mock_distance_sensor.value = 1

        self.system_under_test = FollowControl(self.mock_robot, max_scan_time=TestFollowControl.SCAN_SLEEP,
                                               scan_frequency=0.0)

    def setup_distance_sensor(self, value: float, state: str):
        self.mock_distance_sensor.value = value
        self.mock_distance_sensor.check_state.return_value = state

    def test_detected_guide_then_start_following(self):
        self.setup_distance_sensor(0.3, RobotDistanceSensor.IN_RANGE)
        self.system_under_test.update()

        self.mock_robot.forward.assert_called()

    def test_too_close_then_stop_robot(self):
        self.setup_distance_sensor(0.01, RobotDistanceSensor.TOO_CLOSE)

        self.system_under_test.update()
        self.mock_robot.stop.assert_called()

    def test_not_in_range_then_start_scan(self):
        self.setup_distance_sensor(1, RobotDistanceSensor.OUT_OF_RANGE)

        self.system_under_test.update()
        self.mock_robot.spin_clockwise.assert_called()

    def test_not_in_range_for_long_then_stop_scan(self):
        self.setup_distance_sensor(1, RobotDistanceSensor.OUT_OF_RANGE)
        self.mock_distance_sensor.last_detected = time() - self.system_under_test.max_scan_time - 1
        self.mock_distance_sensor.is_in_range.return_value = False

        self.system_under_test.update()
        self.mock_robot.stop.assert_called()

    def test_already_following_then_dont_follow(self):
        self.setup_distance_sensor(0.3, RobotDistanceSensor.IN_RANGE)

        self.system_under_test.update()
        self.system_under_test.update()

        self.mock_robot.forward.assert_called_once()

    def test_already_scanning_then_dont_scan(self):
        self.setup_distance_sensor(1, RobotDistanceSensor.OUT_OF_RANGE)

        self.system_under_test.update()
        self.system_under_test.update()
        self.mock_robot.spin_clockwise.assert_called_once()

    def test_not_scanning_then_dont_stop_scanning(self):
        self.setup_distance_sensor(1, RobotDistanceSensor.OUT_OF_RANGE)
        self.mock_distance_sensor.last_detected = time() - self.system_under_test.max_scan_time - 1
        self.mock_distance_sensor.is_in_range.return_value = False

        self.system_under_test.update()
        self.system_under_test.update()
        self.mock_robot.stop.assert_called_once()
