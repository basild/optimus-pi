# Robot
These are the core classes that define and control the robot
## Control and Sensor Components
### Optimus Pi
Main domain object that is the heart of the whole robot.
Has control interfaces for movement and accessing the sensors  
and any other actuators under the direct control of the robot.  

If this gets too complicated then can look into moving some of these items control elements in other places
###  Robot
This is the standard Robot object from gpiozero and does the actual moving of the robot. 
It internally controls the motor as well. 
### Line Follower

# Controllers
## Keyboard Control Interface
Allows the control of the robot via the keyboard:

* Movement
* Speed
* Camera - Still to do

## Line Controller Interface
Provides various strategies that can track standard non looping lines

* 2 Sensor - Straddle only
* 3 Sensor - Straddle and Guide
* 4, 6 sensor variants are not yet implemented

## Follow Interface
This will work based on inputs from the distance sensor

* Will follow an obstacle in range
* Will stop if it gets too close
* Will scan if nothing in range
* Will stop scan if nothing found after timeout

## IR Controller Interface 
(Low Priority)
Controller via the remote control
Will have configuration for various actions
## Obstacle Avoidance interface
(Low Priority)
This will try to move in a straight line avoiding obstacles along the way 
TODO: Still to flesh out the design

# Driver
## PyGame Driver
This will be the main driver for the robot
It will allow the various controllers to be wired in and update their state as part of the game loop.

### Display
Enables the robot to be visualized on the game

* Motor Display
* Line Sensor Display
* Distance Sensor Display
* Servo Display for Camera - TODO

## Web Driver
This can be based on flask or some similar interface that renders the controls and the camera in a web page and allows control via a browser

# Indicators
## Turn Indicator
## Sound indicator??
## LCD Matrix panel display
## OLED panel display

# Additional Features
## Camera / Object Tracking
Might not be within the capabilities of a Pi Zero, so would need to mount the pi 4 into it
Desired features would be:

* Move Camera to pan and tilt to keep the object centred
* Move body to normalize camera as centred (i.e. to make body face the robot)
* Follow the face if it got smaller - not sure if this is possible