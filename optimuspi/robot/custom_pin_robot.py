from gpiozero import Motor


# This class is not ready, and only a placeholder for if we control robot via shift registers or I2C
class CustomPinRobot:
    FORWARD = "FORWARD"
    BACKWARD = "BACKWARD"
    LEFT = "LEFT"
    RIGHT = "RIGHT"
    NONE = "NONE"

    def __init__(self, motor_right, motor_left):
        self.motor_left = motor_left
        self.motor_right = motor_right
        self.speed = 1

    def set_speed(self, value):
        self.speed = value

    def stop(self):
        self.motor_left.stop()
        self.motor_right.stop()

    def forward(self):
        self.motor_left.forward(self.speed)
        self.motor_right.forward(self.speed)

    def backward(self):
        self.motor_left.backward(self.speed)
        self.motor_right.backward(self.speed)

    def spin_clockwise(self):
        self.motor_left.forward(self.speed)
        self.motor_right.backward(self.speed)

    def spin_counterclockwise(self):
        self.motor_left.backward(self.speed)
        self.motor_right.forward(self.speed)

    def forward_left(self):
        self.motor_left.stop()
        self.motor_right.forward(self.speed)

    def forward_right(self):
        self.motor_left.forward(self.speed)
        self.motor_right.stop()

    def backward_left(self):
        self.motor_left.stop()
        self.motor_right.backward(self.speed)

    def backward_right(self):
        self.motor_left.backward(self.speed)
        self.motor_right.stop()
