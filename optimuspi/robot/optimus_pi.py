from time import time
from typing import TypedDict

from gpiozero import Robot, LineSensor, DistanceSensor
import logging


class OptimusPi:
    SPEED_RES = 0.1

    def __init__(self, motor_left_pins=None, motor_right_pins=None, motor_pwm=True,
                 line_sensor_pins: dict = None,
                 distance_trigger_pin=None, distance_echo_pin=None):
        self.logger = logging.getLogger(__name__)
        self.speed = 1

        self.line_sensors = LineSensorGroup(self, line_sensor_pins=line_sensor_pins)
        self.distance_sensor = RobotDistanceSensor(distance_trigger_pin=distance_trigger_pin,
                                                   distance_echo_pin=distance_echo_pin)

        self.robot: Robot = Robot(left=motor_left_pins, right=motor_right_pins, pwm=motor_pwm)

    def is_moving(self) -> bool:
        return self.robot.is_active

    def set_speed(self, value):
        self.speed = max(0.00, min(1.00, value))
        self.logger.info("Speed Set To: " + str(self.speed))

    def increase_speed(self):
        self.speed = min(1.00, self.speed + OptimusPi.SPEED_RES)
        self.logger.info("Speed Set To: " + str(self.speed))

    def decrease_speed(self):
        self.speed = max(0.00, self.speed - OptimusPi.SPEED_RES)
        self.logger.info("Speed Set To: " + str(self.speed))

    def stop(self):
        self.logger.info("Stopping")
        self.robot.stop()

    def forward(self):
        self.logger.info("Moving Forward")
        self.robot.forward(speed=self.speed)

    def backward(self):
        self.logger.info("Moving Backward")
        self.robot.backward(speed=self.speed)

    def spin_clockwise(self):
        self.logger.info("Spinning Clockwise")
        self.robot.right(speed=self.speed)

    def spin_counterclockwise(self):
        self.logger.info("Spinning Counter Clockwise")
        self.robot.left(speed=self.speed)

    def forward_left(self):
        self.logger.info("Moving Forward Left")
        self.robot.forward(speed=self.speed, curve_left=1)

    def forward_right(self):
        self.logger.info("Moving Forward Right")
        self.robot.forward(speed=self.speed, curve_right=1)

    def backward_left(self):
        self.logger.info("Moving Backward Left")
        self.robot.backward(speed=self.speed, curve_left=1)

    def backward_right(self):
        self.logger.info("Moving Backward Right")
        self.robot.backward(speed=self.speed, curve_right=1)


class OptimusPiLineSensor(TypedDict):
    position: str
    sensor: LineSensor


class RobotDistanceSensor:
    NOT_INITIALISED = "NOT_INITIALISED"
    OUT_OF_RANGE = "OUT_OF_RANGE"
    IN_RANGE = "IN_RANGE"
    TOO_CLOSE = "TOO_CLOSE"

    CLOSE_THRESHOLD = 0.2
    RANGE_THRESHOLD = 0.6

    def __init__(self, distance_trigger_pin=None, distance_echo_pin=None):
        self.last_detected = time() - 0.1

        if distance_trigger_pin is not None and distance_echo_pin is not None:
            self.sensor: DistanceSensor = DistanceSensor(echo=distance_echo_pin, trigger=distance_trigger_pin,
                                                         threshold_distance=RobotDistanceSensor.RANGE_THRESHOLD)
        else:
            self.sensor: DistanceSensor = None

    def check_state(self) -> str:
        if not self.sensor_enabled():
            return self.NOT_INITIALISED

        if not self.sensor.is_in_range():
            return self.OUT_OF_RANGE

        self.last_detected = time()
        if self.sensor.is_too_close():
            return self.TOO_CLOSE

        return self.IN_RANGE

    def is_in_range(self) -> bool:
        return self.sensor is not None and self.sensor.value <= self.RANGE_THRESHOLD

    def is_too_close(self) -> bool:
        return self.sensor is not None and self.sensor.value < 0.1

    def sensor_enabled(self) -> bool:
        return self.sensor is not None


# TODO: This name might become GroundSensorGroup, or RobotSensorGroup
class LineSensorGroup:
    def __init__(self, robot: OptimusPi, line_sensor_pins: dict = None):
        self.robot = robot
        if line_sensor_pins is None:
            line_sensor_pins = {}

        self.sensors = {}
        for line_sensor_pin in line_sensor_pins.items():
            self.sensors[line_sensor_pin[0]] = LineSensor(pin=line_sensor_pin[1])  # Might have to pass in active state

    def when_activated(self, method):
        for sensor in self.sensors.values():
            sensor.when_activated = lambda trigger: method(self.robot, trigger)

    def when_deactivated(self, method):
        for sensor in self.sensors.values():
            sensor.when_deactivated = lambda trigger: method(self.robot, trigger)

    def has_all_positions(self, expected_positions=None) -> bool:
        if expected_positions is None:
            expected_positions = []
        return all(position in self.sensors.keys() for position in expected_positions)

    def active(self, position) -> bool:
        return self.all_active(position)

    def inactive(self, position) -> bool:
        return self.all_inactive(position)

    def all_active(self, *args) -> bool:
        result = True
        for position in args:
            result = result and self.sensors[position].is_active

        return result

    def all_inactive(self, *args) -> bool:
        result = False
        for position in args:
            result = result or self.sensors[position].is_active

        return not result

    def num_sensors(self) -> int:
        return len(self.sensors)
