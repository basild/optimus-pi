from gpiozero import Robot
from pgzero.builtins import keys
from pgzero.constants import keys

from optimuspi.robot.optimus_pi import OptimusPi


class KeyboardControl:
    PRESSED = 1
    NOT_PRESSED = 0

    def __init__(self, robot: OptimusPi, left=keys.LEFT, right=keys.RIGHT, forward=keys.UP, backward=keys.DOWN,
                 speed_inc=keys.W, speed_dec=keys.S):
        self.robot = robot
        self.left = left
        self.right = right
        self.forward = forward
        self.backward = backward
        self.keys_state = {left: KeyboardControl.NOT_PRESSED, right: KeyboardControl.NOT_PRESSED,
                           forward: KeyboardControl.NOT_PRESSED, backward: KeyboardControl.NOT_PRESSED}
        self.speed_dec = speed_dec
        self.speed_inc = speed_inc

    def on_key_down(self, key_down):
        if not self._update_state_if_relevant(key_down, KeyboardControl.PRESSED):
            return
        self._process_move_controls()

    def on_key_up(self, key_up):
        self._process_speed_controls(key_up)

        if not self._update_state_if_relevant(key_up, KeyboardControl.NOT_PRESSED):
            return

        self._process_move_controls()

    def _process_move_controls(self):
        down = KeyboardControl.PRESSED
        up = KeyboardControl.NOT_PRESSED

        if self._check_state(down, up, up, up):
            self.robot.forward()
        elif self._check_state(up, down, up, up):
            self.robot.backward()
        elif self._check_state(up, up, down, up):
            self.robot.spin_counterclockwise()
        elif self._check_state(up, up, up, down):
            self.robot.spin_clockwise()
        elif self._check_state(down, up, down, up):
            self.robot.forward_left()
        elif self._check_state(down, up, up, down):
            self.robot.forward_right()
        elif self._check_state(up, down, down, up):
            self.robot.backward_left()
        elif self._check_state(up, down, up, down):
            self.robot.backward_right()
        else:
            self.robot.stop()

    def _update_state_if_relevant(self, curr_key, curr_state):
        if curr_key not in self.keys_state:
            return False
        if self.keys_state[curr_key] == curr_state:
            return False

        self.keys_state[curr_key] = curr_state
        return True

    def _check_state(self, forward_state, backward_state, left_state, right_state):
        return self.keys_state[self.forward] == forward_state and self.keys_state[self.backward] == backward_state and \
               self.keys_state[self.left] == left_state and self.keys_state[self.right] == right_state

    def _process_speed_controls(self, key_up):
        if key_up == self.speed_inc:
            self.robot.increase_speed()
        elif key_up == self.speed_dec:
            self.robot.decrease_speed()
