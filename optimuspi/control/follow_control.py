import logging
from time import time

from optimuspi.robot.optimus_pi import OptimusPi, RobotDistanceSensor


class FollowControl:
    SCAN_SPEED = 0.5
    FOLLOW_SPEED = 1
    SCAN_TIMEOUT = 5
    SCAN_FREQUENCY = 0.5

    def __init__(self, robot: OptimusPi, enable: bool = True, max_scan_time: float = SCAN_TIMEOUT,
                 scan_frequency: float = SCAN_FREQUENCY):
        self.logger = logging.getLogger(__name__)

        self.enable = enable
        self.robot = robot
        self.distance_sensor = self.robot.distance_sensor
        self.max_scan_time = max_scan_time
        self.scan_frequency = scan_frequency

        self.scanning = True
        self.scan_state = RobotDistanceSensor.NOT_INITIALISED
        self.last_scanned = time() - self.scan_frequency

        if self.enable:
            self.update()

    def update(self):
        if not self.enable:
            return

        if self.last_scanned + self.scan_frequency > time():
            return

        self.last_scanned = time()
        new_state = self.distance_sensor.check_state()

        if new_state != self.scan_state:
            if new_state == RobotDistanceSensor.TOO_CLOSE:
                self._stop_following()
            elif new_state == RobotDistanceSensor.IN_RANGE:
                self._start_following()
            elif new_state == RobotDistanceSensor.OUT_OF_RANGE:
                self._start_scanning()

        self._stop_on_failed_search()
        self.scan_state = new_state

    def _start_scanning(self):
        self.scanning = True
        self.logger.info("No Guide Detected - Scanning Area - Sensor: " + str(self.distance_sensor.sensor.value))
        self.robot.set_speed(FollowControl.SCAN_SPEED)
        self.robot.spin_clockwise()

    def _stop_on_failed_search(self) -> bool:
        if not self.scanning:
            return True

        if (not self.distance_sensor.is_in_range()) and \
                time() - self.distance_sensor.last_detected > self.max_scan_time:
            self._stop_scanning()
            return True

        return False

    def _stop_scanning(self):
        self.scanning = False
        self.logger.info("No Guide Detected - Scan timed out - Stopping - Sensor: " + 
                         str(self.distance_sensor.sensor.value))
        self.robot.stop()

    def _start_following(self):
        self.scanning = True
        self.logger.info("Guide Detected - Moving Ahead - Sensor: " + str(self.distance_sensor.sensor.value))
        self.robot.set_speed(FollowControl.FOLLOW_SPEED)
        self.robot.forward()

    def _stop_following(self):
        if self.robot.is_moving:
            self.logger.info("Guide too close - Stopping - Sensor: " + str(self.distance_sensor.sensor.value))
            self.robot.stop()
