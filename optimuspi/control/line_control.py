import logging
from threading import Thread
from time import sleep

from gpiozero import LineSensor

from optimuspi.robot.optimus_pi import OptimusPi, LineSensorGroup


class SensorStrategy:
    def __init__(self, expected_positions: list, robot: OptimusPi, enable: bool = True):
        self.logger = logging.getLogger(__name__)
        self.expected_positions = expected_positions
        self.enable = enable

        available_sensors = robot.line_sensors.sensors
        if not all(position in available_sensors.keys() for position in self.expected_positions):
            raise ValueError("Expected sensors in: " + str(self.expected_positions) +
                             " but received: " + str(available_sensors.keys()))

        self.robot: OptimusPi = robot
        self.sensors: LineSensorGroup = self.robot.line_sensors
        self.distance_sensor = self.robot.distance_sensor

    def _motion(self, description, motion_method):
        if not self.enable:
            return

        if self.distance_sensor.is_too_close():
            # TODO: Not working - need to call update state to check for closeness outside of state changes
            self.logger.info("Cancelling Motion to avoid collision. Retrying in a while")
            Thread(target=self._retry_motion, args=(description, motion_method)).start()
            return

        self.logger.info(description)
        motion_method()

    def _retry_motion(self, description, motion_method):
        sleep(5)
        self._motion(description, motion_method)

    def sensor_activated(self, robot: OptimusPi, sensor: LineSensor):
        self.sensor_status_changed(robot, sensor)

    def sensor_deactivated(self, robot: OptimusPi, sensor: LineSensor):
        self.sensor_status_changed(robot, sensor)

    def sensor_status_changed(self, robot: OptimusPi, sensor: LineSensor):
        self.logger.info("No Strategy Configured - No action taken")


class OnlyStraddleSensorStrategy(SensorStrategy):
    """
        Requires 2 pins configured in left and right positions

        The distance between the two pins should be a little wider than the line
    """
    def __init__(self, robot: OptimusPi, enable: bool = True):
        super(OnlyStraddleSensorStrategy, self).__init__(["left", "right"], robot, enable=enable)

    def sensor_status_changed(self, robot: OptimusPi, sensor: LineSensor):
        if self.sensors.all_inactive("left", "right"):
            super()._motion("On Track - Straight Ahead", robot.forward)
        elif self.sensors.active("left") and self.sensors.inactive("right"):
            super()._motion("Straying to the right - Gentle Left", robot.forward_left)
        elif self.sensors.active("right") and self.sensors.inactive("left"):
            super()._motion("Straying to the left - Gentle Right", robot.forward_right)
        else:
            super()._motion("No valid sensor combination found. - I'm Lost :( - ", robot.stop)


class StraddleAndGuideSensorStrategy(SensorStrategy):
    """
        Requires 3 pins configured in left, right and centre-front positions

        The distance between adjacent pins should be less than the width of the line
        but the left and right pins should far enough as to not be on the line when placed normally
    """
    def __init__(self, robot: OptimusPi, enable: bool = True):
        super(StraddleAndGuideSensorStrategy, self).__init__(["left", "right", "centre-front"], robot, enable=enable)

    def sensor_status_changed(self, robot: OptimusPi, sensor: LineSensor):
        if self.sensors.active("centre-front") and self.sensors.all_inactive("left", "right"):
            super()._motion("On Track - Straight Ahead - centre-front", robot.forward)
        elif self.sensors.all_active("left", "centre-front") and self.sensors.inactive("right"):
            super()._motion("Straying to the right - Gentle Left - left/centre-front", robot.forward_left)
        elif self.sensors.all_active("right", "centre-front") and self.sensors.inactive("left"):
            super()._motion("Straying to the left - Gentle Right - right/centre-front", robot.forward_right)
        elif self.sensors.active("left") and self.sensors.all_inactive("centre-front", "right"):
            super()._motion("Way off to the right - Sharp  Left - left", robot.spin_counterclockwise)
        elif self.sensors.active("right") and self.sensors.all_inactive("centre-front", "left"):
            super()._motion("Way off to the left - Sharp  Right - right", robot.spin_clockwise)
        else:
            super()._motion("No valid sensor combination found. - I'm Lost :( - ", robot.stop)


class StraddleAndDoubleGuideSensorStrategy(SensorStrategy):
    def __init__(self, robot: OptimusPi, enable: bool = True):
        super(StraddleAndDoubleGuideSensorStrategy, self).__init__(["left", "right", "centre-front", "centre-back"],
                                                                   robot, enable=enable)


class WingStraddleAndDoubleGuideSensorStrategy(SensorStrategy):
    def __init__(self, robot: OptimusPi, enable: bool = True):
        super(WingStraddleAndDoubleGuideSensorStrategy, self).__init__(["left", "right", "centre-front", "centre-back",
                                                                        "wing-left", "wing-right"], robot,
                                                                       enable=enable)


class LinearLineControl:
    def __init__(self, robot: OptimusPi, strategy_name: str, enable: bool = True):
        self.enable = enable
        self.robot = robot

        if strategy_name == "OnlyStraddleSensorStrategy":
            self.sensor_strategy = OnlyStraddleSensorStrategy(self.robot, enable=self.enable)
        elif strategy_name == "StraddleAndGuideSensorStrategy":
            self.sensor_strategy = StraddleAndGuideSensorStrategy(self.robot, enable=self.enable)
        elif strategy_name == "StraddleAndDoubleGuideSensorStrategy":
            self.sensor_strategy = StraddleAndDoubleGuideSensorStrategy(self.robot, enable=self.enable)
        elif strategy_name == "WingStraddleAndDoubleGuideSensorStrategy":
            self.sensor_strategy = WingStraddleAndDoubleGuideSensorStrategy(self.robot, enable=self.enable)
        else:
            raise ValueError("Unknown strategy type: " + strategy_name)

        self.robot.line_sensors.when_activated(self.sensor_strategy.sensor_activated)
        self.robot.line_sensors.when_deactivated(self.sensor_strategy.sensor_deactivated)
