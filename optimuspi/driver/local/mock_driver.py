from time import sleep, perf_counter

from gpiozero import Device, SPI, SPIBadChannel
from gpiozero.devices import GPIOMeta, GPIOBase
from gpiozero.pins.mock import MockFactory, MockPWMPin, MockTriggerPin, MockChargingPin
from pgzero.keyboard import Keyboard
from pgzero.screen import SurfacePainter


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class SingletonGPIOMeta(SingletonMeta, GPIOMeta):
    pass


class PreciseMockTriggerPin(MockTriggerPin, MockPWMPin):
    def _echo(self):
        sleep(0.001)
        self.echo_pin.drive_high()

        # sleep(), time() and monotonic() dont have enough precision!
        init_time = perf_counter()
        while True:
            if perf_counter() - init_time >= self.echo_time:
                break

        self.echo_pin.drive_low()


class MockSPI(SPI, metaclass=SingletonGPIOMeta if issubclass(SPI, GPIOBase) else SingletonMeta):

    def __init__(self, *args, **kwargs):
        self._values = {}
        self.device_code = 3008

    def close(self):
        pass

    def _int_to_words(self, pattern):
        if self.device_code in [3001, 3002, 3201, 3301]:
            bits_required = 16
        else:
            bits_required = 24

        shifts = range(0, bits_required, self.bits_per_word)[::-1]
        mask = 2 ** self.bits_per_word - 1

        return [(pattern >> shift) & mask for shift in shifts]

    def _get_channel(self, data):
        if self.device_code in [3001, 3201, 3301]:
            return 0
        elif self.device_code == 3002:
            return (data[0] >> 4) & 0b0001
        elif self.device_code == 3202:
            return (data[1] >> 6) & 0b0001
        elif self.device_code in [3004, 3008]:
            return (data[1] >> 4) & 0b0111
        elif self.device_code in [3204, 3208]:
            return (data[0] & 1) << 2 | data[1] >> 6
        elif self.device_code in [3302, 3304]:
            return (data[0] & 0b11) << 1 | data[1] >> 7

    def _get_bit_resolution(self):
        if self.device_code // 100 == 30:
            return 10
        else:
            return 12

    def transfer(self, data):
        channel = self._get_channel(data)
        value = self._values.get(channel, 0)
        bits = self._get_bit_resolution()
        min_value = -(2 ** bits)
        value_range = 2 ** (bits + 1) - 1
        int_value = int((value + 1) * value_range / 2 + min_value)

        if self.device_code == 3001:
            int_value = int_value << 3
        elif self.device_code == 3201:
            int_value = int_value << 1

        return self._int_to_words(int_value)

    def set_value_for_channel(self, value, channel):
        max_channel = self.device_code % 10 - 1
        if not isinstance(channel, int) or channel < 0 or channel > max_channel:
            raise SPIBadChannel("channel must be between 0 and %d" % max_channel)

        self._values[channel] = value


class PreciseMockFactory(MockFactory):
    def __init__(self, revision=None, pin_class=None):
        super(PreciseMockFactory, self).__init__(revision=revision, pin_class=pin_class)
        self.spi_classes = {
            ('hardware', 'exclusive'): MockSPI,
            ('hardware', 'shared'): MockSPI,
            ('software', 'exclusive'): MockSPI,
            ('software', 'shared'): MockSPI,
        }

    def spi(self, **spi_args):
        return MockSPI()

    @staticmethod
    def ticks():
        # time() and monotonic() dont have enough precision!
        return perf_counter()


class PreciseMockChargingPin(MockChargingPin, MockPWMPin):

    def _charge(self):
        init_time = perf_counter()
        while True:
            if perf_counter() - init_time >= self.charge_time:
                break

        try:
            self.drive_high()
        except AssertionError:
            pass

    pass


class MockDistanceSensor:
    DIST_RES = 1

    def __init__(self, trigger_pin, echo_pin, min_distance=0, max_distance=100):
        self.max_distance = max_distance
        self.min_distance = min_distance
        self.distance = self.max_distance
        self._echo_pin = Device.pin_factory.pin(echo_pin)
        self._trigger_pin: PreciseMockTriggerPin = Device.pin_factory.pin(trigger_pin,
                                                                          pin_class=PreciseMockTriggerPin,
                                                                          echo_pin=self._echo_pin,
                                                                          echo_time=0.004)

        self._echo_pin._bounce = 0
        self._trigger_pin._bounce = 0
        self.set_distance(self.distance)

    def set_distance(self, distance):
        self.distance = min(self.max_distance, max(self.min_distance, distance))
        # print("distance set to: " + str(self.distance))
        speed_of_sound = 343.26  # m/s
        _distance = float(self.distance) / 100  # cm -> m
        self._trigger_pin.echo_time = _distance * 2 / speed_of_sound

    def increase_distance(self):
        self.set_distance(self.distance + self.DIST_RES)

    def decrease_distance(self):
        self.set_distance(self.distance - self.DIST_RES)


class MockControl:
    def __init__(self, enable: bool = False):

        self.enable = enable
        if not self.enable:
            return

        self.enable_distance_sensor: bool = False
        self.mock_distance_sensor: MockDistanceSensor = None
        self.increase_key: str = None
        self.decrease_key: str = None

        self.enable_line_sensor: bool = False
        self.mock_line_sensor_key_pin: dict = {}
        self.mock_pin_factory = PreciseMockFactory(pin_class=MockPWMPin)
        Device.pin_factory = self.mock_pin_factory

    def configure_mock_line_sensor(self, key_pin: dict):
        self.mock_line_sensor_key_pin = key_pin
        self.enable_line_sensor = True

    def configure_mock_distance_sensor(self, trigger_pin: int = None, echo_pin: int = None,
                                       increase_key: str = None, decrease_key: str = None):
        self.increase_key = increase_key
        self.decrease_key = decrease_key

        self.mock_distance_sensor = MockDistanceSensor(trigger_pin, echo_pin)
        self.enable_distance_sensor = True

    def draw(self, surface: SurfacePainter, offsets=None):
        # TODO: Can draw some of the mock elements?
        pass

    def fake_sensor_inputs(self, keyboard: Keyboard, pin_factory: MockFactory = None):
        if not self.enable:
            return

        if self.enable_line_sensor:
            for key, pin in self.mock_line_sensor_key_pin.items():
                self._fake_pin_input(keyboard, key, pin_factory.pin(pin))

        if self.enable_distance_sensor:
            self._fake_distance_input(keyboard)

    @staticmethod
    def _fake_pin_input(keyboard: Keyboard, key, pin: MockPWMPin):
        if keyboard[key]:
            pin.drive_high()
        else:
            pin.drive_low()

    def _fake_distance_input(self, keyboard: Keyboard):
        if keyboard[self.increase_key]:
            self.mock_distance_sensor.increase_distance()
        elif keyboard[self.decrease_key]:
            self.mock_distance_sensor.decrease_distance()
