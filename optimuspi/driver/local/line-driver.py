import datetime
import logging
import time

from gpiozero import Device, PWMOutputDevice, DigitalInputDevice
from gpiozero.pins.pigpio import PiGPIOFactory

logging.basicConfig(format='%(asctime)-15s %(module)-15s %(message)s', level=logging.DEBUG)

if Device.pin_factory is None:
    Device.pin_factory = PiGPIOFactory(host="pi-3A-01")

right_forward_pin = PWMOutputDevice(16)
right_backward_pin = PWMOutputDevice(12)

left_forward_pin = PWMOutputDevice(20)
left_backward_pin = PWMOutputDevice(21)

left_wing_sensor = DigitalInputDevice(pin=26)  # Left: 19, Left Wing: 26
left_sensor = DigitalInputDevice(pin=19)
centre_sensor = DigitalInputDevice(pin=13)
right_sensor = DigitalInputDevice(pin=6)  # Right: 6, Right Wing: 5
right_wing_sensor = DigitalInputDevice(pin=5)  # Right: 6, Right Wing: 5


def move_robot(left_front, right_front, left_back, right_back):
    left_forward_pin.value = left_front
    left_backward_pin.value = left_back
    right_forward_pin.value = right_front
    right_backward_pin.value = right_back


def main_loop():
    left_time = datetime.datetime.now()
    right_time = datetime.datetime.now()

    # fps = 0;
    while True:
        start_time = time.time()
        left_wing_sensor_status: bool = left_wing_sensor.is_active
        left_sensor_status: bool = left_sensor.is_active
        right_sensor_status: bool = right_sensor.is_active
        right_wing_sensor_status: bool = right_wing_sensor.is_active
        centre_sensor_status: bool = centre_sensor.is_active

        if left_wing_sensor_status:
            left_time = datetime.datetime.now()
        if right_wing_sensor_status:
            right_time = datetime.datetime.now()

        max_speed = 0.5
        mid_speed = 0.4
        min_speed = 0.3

        if centre_sensor_status and not any([left_wing_sensor_status, right_wing_sensor_status,
                                             left_sensor_status, right_sensor_status]):
            logging.info("On Track - Straight Ahead - centre-front")
            move_robot(max_speed, max_speed, 0, 0)
        elif all([centre_sensor_status, left_sensor_status]) \
                and not any([left_wing_sensor_status, right_wing_sensor_status, right_sensor_status]):
            logging.info("Straying Slightly to Right - Slight curve left - left/centre-front")
            move_robot(mid_speed, max_speed, 0, 0)
        elif all([centre_sensor_status, right_sensor_status]) \
                and not any([left_wing_sensor_status, right_wing_sensor_status, left_sensor_status]):
            logging.info("Straying Slightly to Left - Slight curve right - right/centre-front")
            move_robot(max_speed, mid_speed, 0, 0)
        elif all([left_sensor_status, left_wing_sensor_status]) \
                and not any([right_wing_sensor_status, right_sensor_status]):
            logging.info("Straying more to Right - More Curve left - left/centre-front")
            move_robot(0, max_speed, 0, 0)
        elif all([right_sensor_status, right_wing_sensor_status]) \
                and not any([left_wing_sensor_status, left_sensor_status]):
            logging.info("Straying more to Left - More Curve right - right/centre-front")
            move_robot(max_speed, 0, 0, 0)
        elif all([centre_sensor_status, left_wing_sensor_status]) and not right_wing_sensor_status:
            logging.info("Straying to the right - Gentle Left - left-wing/centre-front")
            move_robot(0, min_speed, 0, 0)
        elif all([centre_sensor_status, right_wing_sensor_status]) and not left_wing_sensor_status:
            logging.info("Straying to the left - Gentle Right - right/centre-front")
            move_robot(min_speed, 0, 0, 0)
        elif left_wing_sensor_status and not any([centre_sensor_status, right_wing_sensor_status]):

            logging.info("Way off to the right - Sharp  Left - left")
            move_robot(0, min_speed, min_speed, 0)
        elif right_wing_sensor_status and not any([centre_sensor_status, left_wing_sensor_status]):
            logging.info("Way off to the left - Sharp  Right - right")
            move_robot(min_speed, 0, 0, min_speed)
        else:
            logging.info("No valid sensor combination found. - I'm Lost :( - ")
            move_robot(0, 0, 0, 0)
            logging.info("left: " + str(left_time) + " - right: " + str(right_time))
            if left_time < right_time:
                move_robot(0, 0, 0, min_speed)
            elif right_time < left_time:
                move_robot(0, 0, min_speed, 0)

        # sleep(0.1)
        time_diff = (time.time() - start_time)
        fps = 1 / time_diff
        print("FPS: " + str(fps) + " Frame time: " + str(time_diff))


try:
    main_loop()
finally:
    logging.info("Ending Program - Stopping Robot")
    move_robot(0, 0, 0, 0)
