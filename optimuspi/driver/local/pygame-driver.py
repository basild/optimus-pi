import logging
from time import sleep, perf_counter

import pgzrun
from gpiozero import Device, SPI, SPIBadChannel
from gpiozero.devices import GPIOMeta, GPIOBase
from gpiozero.pins.mock import MockFactory, MockPWMPin, MockTriggerPin, MockChargingPin
from gpiozero.pins.pigpio import PiGPIOFactory
from pgzero.keyboard import Keyboard
from pgzero.screen import Screen

from optimuspi.control.follow_control import FollowControl
from optimuspi.control.keyboard_control import KeyboardControl
from optimuspi.control.line_control import LinearLineControl
from optimuspi.driver.local.mock_driver import MockControl
from optimuspi.driver.local.render import RobotDisplay
from optimuspi.robot.optimus_pi import OptimusPi

logging.basicConfig(format='%(asctime)-15s %(module)-15s %(message)s', level=logging.DEBUG)

WIDTH = 400
HEIGHT = 200
TITLE = "Optimus Pi - Robot Display Panel"

mock_controller = MockControl(enable=True)
mock_controller.configure_mock_line_sensor({"z": 22, "x": 27, "c": 17})
mock_controller.configure_mock_distance_sensor(trigger_pin=24, echo_pin=23, increase_key="q", decrease_key="e")

if Device.pin_factory is None:
    Device.pin_factory = PiGPIOFactory(host="pi-3A-01")

optimus_pi = OptimusPi(motor_left_pins=(20, 21), motor_right_pins=(16, 12),
                       line_sensor_pins={"left": 19, "centre-front": 13, "right": 6},
                       distance_echo_pin=24, distance_trigger_pin=23)

optimus_pi.set_speed(0.3)
keyboard_controller = KeyboardControl(optimus_pi)
line_controller = LinearLineControl(optimus_pi, "StraddleAndGuideSensorStrategy", enable=False)
follow_controller = FollowControl(optimus_pi, enable=False)

robotRenderer = RobotDisplay(optimus_pi)


def draw():
    _draw(screen=screen)
    pass


def update():
    mock_controller.fake_sensor_inputs(keyboard=keyboard, pin_factory=Device.pin_factory)
    follow_controller.update()
    pass


def _draw(screen: Screen):
    screen.clear()
    robotRenderer.draw(surface=screen.draw, offsets=(150, 50))


def on_key_up(key):
    keyboard_controller.on_key_up(key)
    pass


def on_key_down(key):
    keyboard_controller.on_key_down(key)
    pass


pgzrun.go()
