from colorzero import Color
from gpiozero import Motor, DistanceSensor, LineSensor, Servo
from pgzero.screen import SurfacePainter
from pygame import Rect

from optimuspi.robot.optimus_pi import OptimusPi, RobotDistanceSensor


class RobotDisplay:
    def __init__(self, robot: OptimusPi):
        self.robot = robot
        self.motor_display_left = MotorDisplay(motor=robot.robot.left_motor)
        self.motor_display_right = MotorDisplay(motor=robot.robot.right_motor)

        self.line_sensor_displays = {}
        for position, sensor in robot.line_sensors.sensors.items():
            self.line_sensor_displays[position] = LineSensorDisplay(sensor=sensor)

        self.distance_sensor_display = DistanceSensorDisplay(robot.distance_sensor)
        self.width = 40
        self.height = 60

    def draw(self, surface: SurfacePainter, offsets=None):
        surface.rect(Rect(offsets[0], offsets[1], self.width+30+30, self.height+60+10), (128, 128, 128))
        main_body_offsets = (30+offsets[0], 60+offsets[1])
        surface.rect(Rect(main_body_offsets[0], main_body_offsets[1],
                          self.width, self.height), (128, 128, 128))
        self.motor_display_left.draw(surface, (main_body_offsets[0] - self.motor_display_left.width - 5,
                                               main_body_offsets[1] + 5))
        self.motor_display_right.draw(surface, (main_body_offsets[0] + self.width + 5,
                                                main_body_offsets[1] + 5))

        self.draw_line_sensor("centre-front", surface, (main_body_offsets[0] + self.width / 2,
                                                        main_body_offsets[1] + 10))

        self.draw_line_sensor("left", surface, (main_body_offsets[0] + self.width / 2 - 10,
                                                main_body_offsets[1] + 10))

        self.draw_line_sensor("right", surface, (main_body_offsets[0] + self.width / 2 + 10,
                                                 main_body_offsets[1] + 10))

        self.distance_sensor_display.draw(surface, (main_body_offsets[0] + self.width / 2 -
                                                    self.distance_sensor_display.width/2,
                                                    main_body_offsets[1] - 5 - self.distance_sensor_display.height))

    def draw_line_sensor(self, position, surface, offsets):
        if position not in self.line_sensor_displays:
            return

        sensor_display = self.line_sensor_displays[position]

        sensor_display.draw(surface, offsets)


class MotorDisplay:
    FORWARD_COLOR = (0, 255, 0)
    BACKWARD_COLOR = (255, 0, 0)

    def __init__(self, motor: Motor):
        self.motor = motor

        self.width = 15
        self.height = 50

    def draw(self, surface: SurfacePainter, offsets=None):
        mid_point_y = offsets[1] + self.height / 2

        surface.rect(Rect(offsets[0], offsets[1], self.width, self.height), (128, 128, 128))
        surface.line((offsets[0], mid_point_y), (offsets[0] + self.width - 1, mid_point_y), (128, 128, 128))
        rect = Rect(offsets[0]+1, mid_point_y, self.width-2, self.motor.value * self.height / 2 * -1)
        rect.normalize()
        surface.filled_rect(rect, MotorDisplay.FORWARD_COLOR if self.motor.value >= 0 else MotorDisplay.BACKWARD_COLOR)


class DistanceSensorDisplay:
    def __init__(self, sensor: RobotDistanceSensor):
        self.sensor = sensor.sensor

        self.width = 8
        self.height = 50

    def draw(self, surface: SurfacePainter, offsets=None):
        value = self.sensor.value
        # surface.rect(Rect(offsets[0], offsets[1], self.width, self.height), (128, 128, 128))
        rect = Rect(offsets[0]+1, offsets[1] + self.height, self.width-2, 1 + (-1 * self.height * value))
        rect.normalize()

        if value == 1.0:
            color = (0, 0, 255)
        elif self.sensor.in_range:
            color = (0, 255, 0)
        else:
            color = (255, 0, 0)

        surface.filled_rect(rect, color)


class LineSensorDisplay:
    def __init__(self, sensor: LineSensor):
        self.sensor = sensor

    def draw(self, surface: SurfacePainter, offsets=None):
        surface.filled_circle(offsets, 3, (0, 255, 0) if self.sensor.is_active else (255, 0, 0))


class ServoDisplay:
    def __init__(self, servo: Servo):
        self.servo = servo
        # Perhaps a vertical or horizontal, and also some other work

    def draw(self, surface: SurfacePainter, offsets=None):
        # TODO: Add in a drawing of an angle
        # What kind of display for a vertical servo?
        pass


# Do we need this separately??
class EdgeSensorDisplay:
    pass


# Not sure if we need this, but would be very hand to see the state of all the pins
class GpioPinDisplay:
    pass
