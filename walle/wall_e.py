import pgzrun
from gpiozero import LED, Device, DigitalOutputDevice
from gpiozero.pins.pigpio import PiGPIOFactory

WIDTH = 200
HEIGHT = 100

Device.pin_factory = PiGPIOFactory(host="pi-3A-01")

right_forward_pin = DigitalOutputDevice(16)
right_backward_pin = DigitalOutputDevice(12)

left_forward_pin = DigitalOutputDevice(20)
left_backward_pin = DigitalOutputDevice(21)


def draw():
    pass


def update():
    if keyboard.up:
        print("up pressed")
        left_forward_pin.on()
        right_forward_pin.on()
        right_backward_pin.off()
        left_backward_pin.off()
    elif keyboard.down:
        print("down pressed")
        right_backward_pin.on()
        left_backward_pin.on()
        right_forward_pin.off()
        left_forward_pin.off()
    elif keyboard.left:
        print("left pressed")
        right_forward_pin.on()
        left_backward_pin.on()
        right_backward_pin.off()
        left_forward_pin.off()
    elif keyboard.right:
        print("right pressed")
        left_forward_pin.on()
        right_backward_pin.on()
        right_forward_pin.off()
        left_backward_pin.off()
    else:
        left_forward_pin.off()
        right_forward_pin.off()
        left_backward_pin.off()
        right_backward_pin.off()


pgzrun.go()
